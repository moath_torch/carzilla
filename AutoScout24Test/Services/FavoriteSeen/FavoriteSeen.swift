//
//  FavoriteSeen.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/17/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import RxSwift

protocol Seenable {
    func allSeen()
    func addNotSeen(vehicle: Vehicle)
    func addSeen(vehicle: Vehicle)
    var notSeenCount: Variable<Int> {get}
}

class FavoriteSeen: Seenable {
    static let shared = FavoriteSeen()
    var notSeenCount: Variable<Int> = Variable(0)
    let storage = ServicesLocator.container.resolve(StorageService.self)!
    private init() {
        getNotSeenVehicles()
    }
    
    private var notSeen: [Vehicle] = [] {
        didSet {
            notSeenCount.value = notSeen.count
        }
    }
    
    func allSeen() {
        notSeen.forEach {s in
            self.storage.save(operation: { _ in
                s.storedVersion()?.seen = true
            })
        }
        notSeen.removeAll()
    }
    
    func addNotSeen(vehicle: Vehicle) {
        notSeen.append(vehicle)
        storage.save {
            vehicle.storedVersion()?.seen = false
        }
    }
    
    func addSeen(vehicle: Vehicle) {
        if let _index = notSeen.index(where: {$0.iD! == vehicle.iD!}) {
            notSeen.remove(at: _index)
            storage.save {
                vehicle.storedVersion()?.seen = true
            }
        }        
    }
    
    private func getNotSeenVehicles() {
        let savedVehicles = storage.getAll(type:  VehicleStored.self, predicateFormat: "seen == \(false)", 1 as AnyObject)
        let vehicles = savedVehicles.map { Vehicle(JSON: $0.toDictionary())!}
        notSeen = vehicles.map({$0})
    }
}
