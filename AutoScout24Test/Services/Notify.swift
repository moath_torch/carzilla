//
//  Notify.swift
//  WarbaFitness
//
//  Created by Moath_Othman on 5/24/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import RxSwift

enum NotifyKeys: String  {
    case removeFavorite
    case addFavorite
}

struct  Notify {
    private  var notificationCenter: NotificationCenter {
        return NotificationCenter.default
    }
    
    func removeObserver(observer: Any) {
        notificationCenter.removeObserver(observer)
    }
    
    @discardableResult
    func listen(name: NotifyKeys, object: Any? = nil, signal: @escaping (Notification) -> Void) -> NSObjectProtocol {
       let obs =  notificationCenter.addObserver(forName: NSNotification.Name(rawValue: name.rawValue), object: object, queue: nil) { (not) in
            signal(not)
        }
        return obs
    }
    
    func send(name: NotifyKeys, object: Any? = nil) {
        notificationCenter.post(name: NSNotification.Name(rawValue: name.rawValue), object: object)
    }
    
    func listen(_ name: NotifyKeys, object: AnyObject? = nil) -> Observable<Notification> {
        return Observable.create { [weak object] observer in
            let nsObserver = self.listen(name: name, object: object) { notification in
                observer.on(.next(notification))
            }
            return Disposables.create {
                self.removeObserver(observer: nsObserver)
            }
        }
    }
}

