//
//  ServiceLocator.swift
//
//  Created by Moath_Othman on 4/17/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import Swinject


/// Contain the DI container .
/// Type should conform to the resolved protocol if used.
struct ServicesLocator {
    static let container: Container = Container() {
        $0.register(APIClient.self) { _ in AutoScoutAPIClient() }
        $0.register(APIServices.self) { r in
            let client = r.resolve(APIClient.self)!
            return APIServices(api: client)
        }
        $0.register(StorageService.self) { _ in StorageService.shared }
        $0.register(Notify.self) { _ in Notify()}
        $0.register(Seenable.self) { _ in FavoriteSeen.shared }
    }
}
