//
//  AddRemoveFavorite.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/17/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import RxSwift

protocol FavoriteOperationHandler: class {
    func remove(vehicle: Vehicle)
    func add(vehicle: Vehicle)
    func listenToEvent()
    var notifier: Notify {get set}
    var bag: DisposeBag {get}
}

extension FavoriteOperationHandler   {
    func listenToEvent() {
        notifier.listen(.removeFavorite).subscribe(onNext: { [unowned self](not) in
            if let _object = not.object as? Vehicle {
                self.remove(vehicle: _object)
            }
        }).addDisposableTo(bag)
        
        notifier.listen(.addFavorite).subscribe(onNext: { [unowned self](not) in
            if let _object = not.object as? Vehicle {
                self.add(vehicle: _object)
            }
        }).addDisposableTo(bag)
    }
}
