//
//  StorageService.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import PromiseKit
import RealmSwift

class StorageService {
    
    static let shared = StorageService()
    
    private var store: Realm {
        return try! Realm()
    }
    
    func getAll<T>(type: T.Type, predicateFormat: String, _ args: AnyObject...) -> [T] where T : Object {
        return self.store.objects(type).filter(predicateFormat, args).toArray()
    }
    
    func getAll<T>(type: T.Type) -> Results<T> {
        return self.store.objects(type)
    }
    
    func delete<T: Object>(object: T) {
        store.beginWrite()
        store.delete(object)
        try! store.commitWrite()
    }
    
    func save<T: Object>(object: T) {
        store.beginWrite()
        store.add(object, update: true)
        try! store.commitWrite()
    }
    
    func save(operation: () -> Void) {
        try! store.write {
           operation()
        }
    }
    
}

extension Results {
    func toArray() -> [T] {
        return self.map{$0}
    }
}
