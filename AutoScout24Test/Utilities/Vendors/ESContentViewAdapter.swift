//
//  ESContentViewAdapter.swift
//
//  Created by Moath_Othman on 5/12/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import ESTabBarController_swift

class ESContentViewAdpater: ESTabBarItemContentView {
    override var title: String? {
        didSet {
            self.titleLabel.text = nil
            self.updateLayout()
        }
    }
}
