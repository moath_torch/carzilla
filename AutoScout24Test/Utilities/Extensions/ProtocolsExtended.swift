//
//  Protocols.swift
//
//  Created by Moath_Othman on 5/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit


// MARK: - default behavior for apipresentable
extension APIPresentable where Self: UIViewController {
    func requestDidFinish() {
        self.view.isUserInteractionEnabled = true
        ToastMakerAdapter.stopLoader(from: self.view)
    }
    
    func requestDidStart() {
        self.view.isUserInteractionEnabled = false
        ToastMakerAdapter.showLoader(from: self.view)
    }
}

extension ErrorPresentable where Self: UIViewController {
    func show(error msg: String?) {
        self.view.isUserInteractionEnabled = true
        ToastMakerAdapter.showFail(on: self.view, message: msg)
    }
}

extension SuccessPresentable where Self: UIViewController {
    func show(success msg: String?) {
        self.view.isUserInteractionEnabled = true
        ToastMakerAdapter.showSuccess(message: msg) // special case
    }
}

///
extension Presenter {
    /// to Avoid redundancy in api calls
    func normalAPIRequest(view: APIPresentable & ErrorPresentable, apipromise: Promise<Void>) {
        view.requestDidStart()
        apipromise.always {
            view.requestDidFinish()
            }.catch { (e) in
                view.show(error: (e as NSError).localizedDescription)
        }
    }
    
    func normalAPIRequestWithNoLoading(view: ErrorPresentable, apipromise: Promise<Void>) {
        apipromise.always {
            }.catch { (e) in
                view.show(error: (e as NSError).localizedDescription)
        }
    }
}


/// EMPTINESS

protocol EMPTY {
    var isEmpty: Bool {get}
}

extension String: EMPTY {}
extension Int: EMPTY { var isEmpty: Bool { return self == 0 } }
extension Bool: EMPTY { var isEmpty: Bool { return !self } }

extension Optional where Wrapped: EMPTY{
    var isnil: Bool {
        switch self {
        case .none:
            return true
        case let .some(value):
            return value.isEmpty
        }
    }
}


func check<T: EMPTY>(if condition: Bool , then: T? , `else`: T? ) -> T? {
    if condition {
        if then.isnil {
            return `else`
        } else {
            return then
        }
    } else {
        if `else`.isnil {
            return then
        } else {
            return `else`
        }
    }
}


func check<T: EMPTY>(if condition: Bool , then: T , `else`: T ) -> T {
    if condition {
        if then.isEmpty {
            return `else`
        } else {
            return then
        }
    } else {
        if `else`.isEmpty {
            return then
        } else {
            return `else`
        }
    }
}

/// variadic else , if true and empty it will loop through the else and if nothing it will just return then
func check<T: EMPTY>(if condition: Bool , then: T , `else`: T... ) -> T {
    if !then.isEmpty && condition{
        return then
    }
    if let result = `else`.first(where: {!$0.isEmpty}) {
        return result
    }
    return then
}

