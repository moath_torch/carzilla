//
//  FoundationCastings.swift
//
//  Created by Moath_Othman on 6/8/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    var float: Float { return Float(self) }
}

extension Int {
    var cgfloat: CGFloat { return CGFloat(self) }
    var float: Float { return Float(self) }
    var string: String { return String(self) }
    var double: Double { return Double(self) }
}

extension String {
    var double: Double { return Double(self) ?? 0.0 }
    var int: Int { return Int(self) ?? 0 }
    var float: Float { return Float(self) ?? 0.0 }
}

extension Float {
    var double: Double { return Double(self) }
    var string: String { return String(self) }
    var int: Int { return Int(self) }
    var cgfloat: CGFloat { return CGFloat(self)}
}

extension Double {
    var string: String { return String(self)}
    var int: Int {return Int(self)}
}
