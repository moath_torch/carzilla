//
//  ToastMakerAdapter.swift
//
//  Created by Moath_Othman on 11/23/16.
//  Copyright © 2016 Moath. All rights reserved.
//

import Foundation
import Toast_Swift
import UIKit

struct ToastMakerAdapter {
    
    private static var window: UIWindow {
        return ((UIApplication.shared.delegate?.window)!)!
    }
    
    static func showLoader(from view: UIView = window) {
        view.makeToastActivity(.center)
    }
    
    static func stopLoader(from view: UIView = window) {
        view.hideToastActivity()
    }
    
    static func showFail(on view: UIView = window, message msg: String?) {
        if msg == "cancelled" {
            return
        }
        showFail(on: view, message: msg, completed: nil)
    }
    
    static func showFail(on view: UIView? = window, message msg: String?, duration: TimeInterval = 1.0, completed: (() -> Void)?) {
        view?.makeToast(msg, duration: duration, position: .center, title: nil, image: nil, style: nil, completion: { _ in
            completed?()
        })
    }
    
    static func showSuccess(on view: UIView = window, message msg: String?) {
        showSuccess(on: view, message: msg, completed: nil)
    }
    
    static func showSuccess(on view: UIView =  window, message msg: String?, completed: (() -> Void)?) {
        view.makeToast(msg, duration: 1.0, position: .center, title: nil, image: nil, style: nil, completion: { _ in
            completed?()
        })
    }
    
    // MARK: - message
    static func showMessage(on view: UIView =  window, duration: TimeInterval = 2.0, message msg: String?, completed: (() -> Void)? = nil) {
        view.makeToast(msg, duration: duration, position: .center, title: nil, image: nil, style: nil, completion: { _ in
            completed?()
        })
    }
    
    static func showMessage(duration: TimeInterval, message msg: String?) {
        window.makeToast(msg, duration: duration, position: .center, title: nil, image: nil, style: nil, completion: nil)
    }
    
    static func showMessage(duration: TimeInterval, message msg: String?, position: ToastPosition) {
        window.makeToast(msg, duration: duration, position: position, title: nil, image: nil, style: nil, completion: nil)
    }
}

