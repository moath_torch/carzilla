//
//  ImagesCacheAD.swift
//  Porsche
//
//  Created by Moath_Othman on 12/4/16.
//  Copyright © 2016 Moath. All rights reserved.
//
import SDWebImage
import Foundation

extension UIImageView {
    var cache: ImagesCacheAD { return ImagesCacheAD(imgView: self) }
}

fileprivate let placeholder = R.image.noimage()!

struct  ImagesCacheAD {
    private let imgView: UIImageView
    init(imgView: UIImageView) {
        self.imgView = imgView
    }
    
    func setImage(with url: String?) {
        guard  let str = url, let _url = URL(string: str) else {
            imgView.image = placeholder
            return
        }
        imgView.sd_setImage(with: _url, placeholderImage: placeholder)
    }
    
    func setImage(with url: URL?) {
        guard  let _url = url else {
            imgView.image = placeholder
            return
        }
        imgView.sd_setImage(with: _url, placeholderImage: placeholder)
    }
}
