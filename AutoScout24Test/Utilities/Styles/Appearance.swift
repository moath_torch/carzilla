//
//  Appearance.swift
//
//  Created by Moath_Othman on 11/6/16.
//  Copyright © 2016 Moath. All rights reserved.
//

import Foundation
import UIKit

struct AppAppearance {
    static func configure() {
        let attributes: [String: Any] = [
            NSFontAttributeName: R.font.sourceSansProLight(size: 24)!,
            NSForegroundColorAttributeName: UIColor.appBlue
        ]
        UINavigationBar.appearance().titleTextAttributes = attributes
        UINavigationBar.appearance().tintColor = UIColor.appOrange
    }
}


