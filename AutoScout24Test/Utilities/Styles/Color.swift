//
//  Color.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var appOrange: UIColor {
        return UIColor.init(red: 1.0, green: 0.45882, blue: 0.0, alpha: 1.0)
    }
    
    static var appBlue: UIColor {
        return UIColor.init(red: 0.0, green: 0.2039, blue: 0.40784, alpha: 1.0)
    }
}
