//
//  FavoriteListPresenter.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.

import UIKit
import ObjectMapper
import RxSwift

protocol FavoriteListPresentable: Presentable {
    func setTitle(to: String)
    func register(cell withID: String)
    func fillTable<T: CarListTableViewCell>(data: Variable<[Vehicle]>, cellId: String, cellType: T.Type)
}

class FavoriteListPresenter: MVPRPresenter, Presenter, FavoriteOperationHandler {
    weak var view: FavoriteListPresentable!
    let storage: StorageService
    var notifier: Notify
    let seen = ServicesLocator.container.resolve(Seenable.self)!

    required init(view: FavoriteListPresentable?) {
        self.view = view
        self.storage = ServicesLocator.container.resolve(StorageService.self)!
        self.notifier = ServicesLocator.container.resolve(Notify.self)!
    }
    
    let bag = DisposeBag()
    private var vehicles = Variable<[Vehicle]>([])

    func markBooksmarksSeen() {
        seen.allSeen()
    }
    
    func fetchData() {
        view.setTitle(to: R.string.localizable.favorites())
        view.register(cell: "car")
        self.view.fillTable(data: vehicles, cellId: "car", cellType: CarListTableViewCell.self)
        getSavedVehicles()
        listenToEvent()
    }
    
    func getSavedVehicles() {
        let savedVehicles = storage.getAll(type: VehicleStored.self)
        let vehicles = savedVehicles.map { Vehicle(JSON: $0.toDictionary())!}
        self.vehicles.value = vehicles.map({$0})
    }
    
    func remove(vehicle: Vehicle) {
        if let index = vehicles.value.index(where: {$0.iD! == vehicle.iD!}) {
            vehicles.value.remove(at: index)
        }
    }
    
    func add(vehicle: Vehicle) {
        vehicles.value.append(vehicle)
    }
}
