//
//  FavoriteListViewController.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FavoriteListViewController: MVPRViewController<FavoriteListPresenter,FavoriteListRouter> {
    // MARK: - View lifecycle
    @IBOutlet weak var tableView: UITableView!
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.markBooksmarksSeen()
    }
    
    /// to keep cell controllers in mempry
    fileprivate var cellControllers: [Int: FavoriteCellController?] = [:]
}

extension FavoriteListViewController: FavoriteListPresentable, ConfirmPresentable {
    func setTitle(to: String) {
        title = to
    }
    
    func register(cell id: String) {
        tableView.register(R.nib.carListCell(), forCellReuseIdentifier: id)
    }
    
    func fillTable<T>(data: Variable<[Vehicle]>, cellId: String, cellType: T.Type) where T : CarListTableViewCell {
        data.asObservable().bind(to: tableView.rx.items(cellIdentifier: cellId, cellType: cellType)) { [unowned self] index,m,c in
            let controller = FavoriteCellController(model: m, cell: c)
            controller.fill()
            controller.style()
            controller.confirmableController = self
            self.cellControllers[c.hashValue] = controller
            }.addDisposableTo(bag)
    }
}
