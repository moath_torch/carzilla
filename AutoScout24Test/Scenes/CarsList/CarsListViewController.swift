//
//  CarsListViewController.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CarsListViewController: MVPRViewController<CarsListPresenter,CarsListRouter> {
    // MARK: - View lifecycle
    @IBOutlet weak var tableView: UITableView!
    let refreshController = UIRefreshControl()
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(R.nib.carListCell(), forCellReuseIdentifier: "car")
        setDelayed { [weak self] _ in self?.reload(firstTime: true) }
        refreshController.addTarget(self, action: #selector(CarsListViewController.refresh), for: .valueChanged)
        self.tableView?.refreshControl = refreshController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delayed()
        dispose()
    }
    
    func refresh() {
        reload(firstTime: false)
    }
    
    func reload(firstTime: Bool) {
        presenter.reload(firstTime: firstTime)
    }
    
    /// to keep cell controllers in mempry
    fileprivate var cellControllers: [Int: CarlistCellController?] = [:]
    
}

extension CarsListViewController: CarsListPresentable, AlertPresentable {
    func setTitle(to: String) {
        title = to
    }
    
    func fillTable(data: Variable<[Vehicle]>) {
        
        data.asObservable().do(onNext: { (_p) in
            if self.refreshController.isRefreshing {
                self.refreshController.endRefreshing()
            }
        }).bind(to: tableView.rx.items(cellIdentifier: "car", cellType: CarListTableViewCell.self)) { [unowned self] index,m,c in
            let controller = CarlistCellController(model: m, cell: c)
            controller.fill()
            controller.style()
            controller.alertpresenter = self
            self.cellControllers[c.hashValue] = controller
            }.addDisposableTo(bag)
    }
}
