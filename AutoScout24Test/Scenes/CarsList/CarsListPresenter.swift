//
//  CarsListPresenter.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.

import UIKit
import ObjectMapper
import RxSwift


protocol CarsListPresentable: Presentable , APIPresentable, ErrorPresentable{
    func setTitle(to: String)
    func fillTable(data: Variable<[Vehicle]>)
}

class CarsListPresenter: MVPRPresenter, Presenter, FavoriteOperationHandler {

    weak var view: CarsListPresentable!
    let api: APIServices
    var notifier: Notify
    required init(view: CarsListPresentable?) {
        self.view = view
        self.api = ServicesLocator.container.resolve(APIServices.self)!
        self.notifier = ServicesLocator.container.resolve(Notify.self)!
    }
    
    var bag = DisposeBag()
    private var vehicles = Variable<[Vehicle]>([])
    private let makes = ["all","mercedes-benz","bmw","audi"]
    
    func fetchData() {
        view.setTitle(to: R.string.localizable.carsList())
        self.view.fillTable(data: vehicles)
        listenToEvent()
    }
    
    func reload(firstTime: Bool) {
        view.requestDidStart()
        let index = firstTime ? 0 : Int(arc4random() % 4)
        let type = MethodName(rawValue: makes[index])!
        print("TY)E", type)
        api.getCars(type: type) { [unowned self](v : VehiclesResponse?, error: NSError?) in
            if let _error = error {
                self.view.show(error: _error.localizedDescription)
            } else {
                self.vehicles.value = v?.vehicles ?? []
            }
            self.view.requestDidFinish()
        }
        
    }
    
    func remove(vehicle: Vehicle) {
        if let index = vehicles.value.index(where: {$0.iD! == vehicle.iD!}) {
            vehicles.value[index].favorited.value = false
        }
    }
    
    func add(vehicle: Vehicle) {
        if let index = vehicles.value.index(where: {$0.iD! == vehicle.iD!}) {
            vehicles.value[index].favorited.value = true
        }
    }
}
