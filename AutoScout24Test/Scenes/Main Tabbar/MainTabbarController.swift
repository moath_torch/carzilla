//
//  MainTabbarViewController.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.
//

import UIKit
import ESTabBarController_swift

class SpecialTabbar<P: Presenter , R: Router>: ESTabBarController  {
    var presenter: P!
    var router: R!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
    }
}

class MainTabbarController: SpecialTabbar<MainTabbarPresenter,MainTabbarRouter> {
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
}

extension MainTabbarController: Stylable {
    func style() {
        tabBar.barTintColor = UIColor.white
        tabBar.barStyle = UIBarStyle.black
        tabBar.isTranslucent = true
        
    }
}

extension MainTabbarController: MainTabbarPresentable {
    func setupFromViewController(views: [UIViewController]) {
        self.viewControllers = views
    }
}
