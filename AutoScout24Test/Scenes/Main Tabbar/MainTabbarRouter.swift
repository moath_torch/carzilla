//
//  MainTabbarRouter.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.
//


import UIKit

class MainTabbarRouter: MVPRRouter, KnownFromRouter {
    weak var fromView: UIViewController?
    unowned var viewController: MainTabbarController = R.storyboard.mainTabbar.mainTabbarController()!
    
    required init(from viewController: UIViewController?) {
        self.fromView = viewController
        super.init()
        let presenter = MainTabbarPresenter(view: self.viewController)
        self.viewController.presenter = presenter
        self.viewController.router = self
    }
    
    func present() {}
    
    func dismiss() {}
}
