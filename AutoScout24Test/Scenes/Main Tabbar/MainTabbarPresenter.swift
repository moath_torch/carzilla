//
//  MainTabbarPresenter.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright (c) 2017 Moath. All rights reserved.

import UIKit
import ESTabBarController_swift
import RxSwift
protocol MainTabbarPresentable: Presentable {
    func setupFromViewController(views: [UIViewController])

}

class MainTabbarPresenter: MVPRPresenter, Presenter {
    weak var view: MainTabbarPresentable!
    let seen = ServicesLocator.container.resolve(Seenable.self)!
    
    required init(view: MainTabbarPresentable?) {
        self.view = view
    }
    
    let bag = DisposeBag()
    
    func fetchData() {
        let carslist = CarsListRouter(from: nil).viewController
        let favlist = FavoriteListRouter(from: nil).viewController
        let views = [carslist,favlist]
        let favitem = ESTabBarItem.init(TabbarItemBasicContentView(),image: R.image.heartEmpty(), selectedImage: R.image.heartFilled())
        carslist.tabBarItem = ESTabBarItem.init(TabbarItemBasicContentView(),image: R.image.carEmpty(), selectedImage: R.image.carFilled())
        favlist.tabBarItem = favitem
        seen.notSeenCount.asObservable().subscribe(onNext: { (count) in
            favitem.badgeValue = count > 0 ? count.string : nil
        }).addDisposableTo(bag)
        self.view.setupFromViewController(views: views.map({$0.createNavigation().customize()}))
    }
}



extension UIViewController {
    func createNavigation() -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: self)
        return navigationController
    }
}
extension UINavigationController {
    func customize() -> UINavigationController {
        self.navigationBar.barTintColor = .white
        return self
    }
}
