//
//  Starter.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import UIKit

struct Starter {
    
    var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    mutating func start() {
        setRootViewController()
    }
    
    func reset() {
        window?.rootViewController = MainTabbarRouter(from: nil).viewController
    }
    
    /// Starting point of the App
    private mutating func setRootViewController() {
        window = makeNewWindow()
        reset()
        window?.makeKeyAndVisible()
        AppAppearance.configure()
    }
    
    /// Create App Window
    private func makeNewWindow() -> UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.orange
        window.tintColor = UIColor.appOrange
        if let del = UIApplication.shared.delegate {
            (del as! AppDelegate).window = window
        }
        return window
    }
    
}


