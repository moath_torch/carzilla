//
//  APIServices.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import ObjectMapper


enum MethodName: String {
    case all
    case mercedes = "mercedes-benz"
    case bmw
    case audi
    static func make(type: MethodName) -> String {
        return "make=\(type.rawValue).json"
    }
}


struct APIServices {
    let api: APIClient
    
    init(api: APIClient) {
        self.api = api
    }
    
    /// get cuisines all , giving type (regular or not) and search term which is not effective so far
    func getCars<T: Mappable>(type: MethodName, completion: @escaping Completion<T>) {
        api.request(method: MethodName.make(type: type), apiMethod: .get, params: [:], completion: completion)
    }
}
