//
//  APIClient.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

typealias Completion<T> = (T?,NSError?) -> Void

protocol APIClient {
    func request<T: Mappable>(method: String, apiMethod: HTTPMethod, params: [String: Any], completion: @escaping (T?,NSError?) -> Void)
}

class AutoScoutAPIClient: APIClient {
    
    struct Constants {
        static let baseurl = "http://sumamo.de/iOS-TechChallange/api/index/"
    }
    
    var manager: SessionManager
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        manager =  Alamofire.SessionManager(configuration: configuration)
    }
    
    private func url(_ method: String) -> URLConvertible {
        return Constants.baseurl + method
    }
    
    func request<T: Mappable>(method: String, apiMethod: HTTPMethod, params: [String: Any], completion: @escaping (T?,NSError?) -> Void) {
        manager.request(url(method), method: apiMethod, parameters: params).responseObject { (res: DataResponse<T>) in
            print("API Response \(res)")
            switch res.result {
            case .success(let v):
                completion(v, nil)
                break
            case .failure(let e):
                completion(nil, e as NSError)
                break
            }
        }
    }
}

