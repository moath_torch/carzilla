//
//  Vehicles.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import RxSwift

/// Vehicles API Response Model
class VehiclesResponse : Mappable{
    
    var vehicles : [Vehicle]?
    
    class func newInstance(map: Map) -> Mappable?{
        return VehiclesResponse()
    }
    required init?(map: Map){}
    private init(){}
    
    func mapping(map: Map)
    {
        vehicles <- map["vehicles"]
    }
    
}



class Vehicle :  Mappable{
    
    var accidentFree : Bool?
    var address : String?
    var firstRegistration : String?
    var fuelType : String?
    var iD : Int?
    var images : [String]?
    var make : String?
    var mileage : Int?
    var powerKW : Int?
    var price : Int?
    var favorited = Variable<Bool>(false)
    
    let storage = ServicesLocator.container.resolve(StorageService.self)!
    
    
    class func newInstance(map: Map) -> Mappable?{
        return Vehicle()
    }
    required init?(map: Map){}
    init(){}
    
    func mapping(map: Map)
    {
        accidentFree <- map["AccidentFree"]
        address <- map["Address"]
        firstRegistration <- map["FirstRegistration"]
        fuelType <- map["FuelType"]
        iD <- map["ID"]
        images <- map["Images"]
        make <- map["Make"]
        mileage <- map["Mileage"]
        powerKW <- map["PowerKW"]
        price <- map["Price"]
    }
    
    func setAsFavorited()  {
        let isfav = isFavorited()
        let stored = storedVersion()!
        if isfav {
            // remove
            storage.delete(object: stored)
        } else {
            // add
            storage.save(object: stored)
        }
        favorited.value = !isfav
    }
    
    func isFavorited() ->  Bool {
        let vehicle: [VehicleStored] = storage.getAll(type: VehicleStored.self, predicateFormat:  "iD == \(iD!)", iD as AnyObject)
        favorited.value = vehicle.count > 0
        return vehicle.count > 0
    }
    
    func storedVersion() -> VehicleStored? {
        let vehicle: [VehicleStored] = storage.getAll(type: VehicleStored.self, predicateFormat:  "iD == \(iD!)", iD as AnyObject)
        if vehicle.count  == 0  {
            return VehicleStored.fromDictionary(dictionary: self.toJSON())
        }
        return vehicle.first
    }
}

/// Vehicle Model
class VehicleStored : Object {
    
    dynamic var accidentFree : Bool = false
    dynamic var address : String = ""
    dynamic var firstRegistration : String = ""
    dynamic var fuelType : String = ""
    dynamic var iD : Int = 0
    var images  = List<Image>()
    dynamic var make : String = ""
    dynamic var mileage : Int = 0
    dynamic var powerKW : Int = 0
    dynamic var price : Int = 0
    dynamic var seen : Bool = true

    let storage = ServicesLocator.container.resolve(StorageService.self)!
    
    override static func primaryKey() -> String? {
        return "iD"
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func fromDictionary(dictionary: [String:Any]) -> VehicleStored	{
        let this = VehicleStored()
        
        if let accidentFreeValue = dictionary["AccidentFree"] as? Bool{
            this.accidentFree = accidentFreeValue
        }
        if let addressValue = dictionary["Address"] as? String{
            this.address = addressValue
        }
        if let firstRegistrationValue = dictionary["FirstRegistration"] as? String{
            this.firstRegistration = firstRegistrationValue
        }
        if let fuelTypeValue = dictionary["FuelType"] as? String{
            this.fuelType = fuelTypeValue
        }
        if let iDValue = dictionary["ID"] as? Int{
            this.iD = iDValue
        }
        if let imagesArray = dictionary["Images"] as? [String]{
            for item in imagesArray{
                let img = Image()
                img.link = item
                this.images.append(img)
            }
        }
        if let makeValue = dictionary["Make"] as? String{
            this.make = makeValue
        }
        if let mileageValue = dictionary["Mileage"] as? Int{
            this.mileage = mileageValue
        }
        if let powerKWValue = dictionary["PowerKW"] as? Int{
            this.powerKW = powerKWValue
        }
        if let priceValue = dictionary["Price"] as? Int{
            this.price = priceValue
        }
        return this
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any] {
        var dictionary = [String:Any]()
        
        dictionary["AccidentFree"] = accidentFree
        dictionary["Address"] = address
        dictionary["FirstRegistration"] = firstRegistration
        dictionary["FuelType"] = fuelType
        dictionary["ID"] = iD
        var dictionaryElements = [String]()
        for i in 0 ..< images.count {
            dictionaryElements.append(images[i].toDictionary()["link"] as! String)
        }
        dictionary["Images"] = dictionaryElements
        dictionary["Make"] = make
        dictionary["Mileage"] = mileage
        dictionary["PowerKW"] = powerKW
        dictionary["Price"] = price
        return dictionary
    }
    
}


class Image: Object {
    dynamic var link: String = ""
    func toDictionary() -> [String:Any] {
        var dictionary = [String:Any]()
        dictionary["link"] = link
        return dictionary
    }
}
