//
//  CarListTableViewCell.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import UIKit
import RxSwift

class  CarlistCellController: NSObject {
    weak var model: Vehicle?
    weak var cell: CarListTableViewCell?
    let notifier: Notify
    weak var alertpresenter: AlertPresentable?
    let seen = ServicesLocator.container.resolve(Seenable.self)!

    required init(model: Vehicle , cell: CarListTableViewCell) {
        self.model = model
        self.cell = cell
        self.notifier = ServicesLocator.container.resolve(Notify.self)!
    }
    
    let bag = DisposeBag()
    
    func fill() {
        cell?.heart.removeTarget(self, action: #selector(heartbuttonTapped), for: .touchUpInside)

        cell?.makeTitle.text = R.string.localizable.make()
        cell?.priceTitle.text = R.string.localizable.price()
        cell?.fuelTitle.text = R.string.localizable.fuelType()
        cell?.milageTitle.text = R.string.localizable.milage()
        cell?.heart.setImage(R.image.heartEmpty(), for: .normal)
        cell?.heart.setImage(R.image.heartFilled(), for: .selected)
        
        cell?.milage.text =  model?.mileage?.string
        cell?.price.text = "$" + (model?.price!.string ?? "")
        cell?.fuelType.text = model?.fuelType
        cell?.make.text = model?.make
        cell?.heart.addTarget(self, action: #selector(heartbuttonTapped), for: .touchUpInside)
        cell?.heart.isSelected = model?.isFavorited() ?? false
        
        cell?.coverImage.cache.setImage(with: model?.images?.first)
        model?.favorited.asObservable().subscribe(onNext: { [unowned self](fav) in
            self.cell?.heart.isSelected = fav
        }).addDisposableTo(bag)
        
        if let _m = model , !_m.accidentFree.isnil {
            cell?.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        } else {
            cell?.backgroundColor = .clear
        }
    }
    
    func heartbuttonTapped(sender: Any) {
        guard let _m = model else { return }
        if !_m.accidentFree.isnil {
            alertpresenter?.alert(title: R.string.localizable.thisCanIsNotAccidentFreeLetsKeepItAway(), ok: R.string.localizable.ok(), okayed: {})
        } else {
            _m.setAsFavorited()
            if _m.favorited.value {
                seen.addNotSeen(vehicle: _m)
            } else {
                seen.addSeen(vehicle: _m)
            }
            
            notify()
        }
    }
    
    func notify() {
        guard  let _selected = cell?.heart.isSelected else {
            return
        }
        if _selected {
           self.notifier.send(name: .addFavorite, object: model)
        } else {
           self.notifier.send(name: .removeFavorite, object: model)
        }
    }
    
    func style() {
        [cell?.fuelTitle, cell?.makeTitle].forEach {
            $0?.font = R.font.sourceSansProRegular(size: 12)
            $0?.textColor = UIColor.appOrange
        }
        
        [cell?.priceTitle, cell?.milageTitle].forEach {
            $0?.font = R.font.sourceSansProRegular(size: 12)
            $0?.textColor = UIColor.appBlue
            $0?.minimumScaleFactor = 0.5
            $0?.adjustsFontSizeToFitWidth = true
        }
        
        [cell?.make,cell?.fuelType, cell?.price, cell?.milage].forEach {
            $0?.font = R.font.sourceSansProLight(size: 28)
            $0?.textColor = UIColor.white
            $0?.minimumScaleFactor = 0.5
            $0?.adjustsFontSizeToFitWidth = true
        }
    }
    
    func remove() {
        cell?.heart.removeTarget(self, action: #selector(heartbuttonTapped), for: .touchUpInside)
        cell = nil
        model = nil
    }
}


class CarListTableViewCell: UITableViewCell {
    // MARK: - outlets
    @IBOutlet weak var makeTitle: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var priceTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var fuelTitle: UILabel!
    @IBOutlet weak var fuelType: UILabel!
    @IBOutlet weak var milageTitle: UILabel!
    @IBOutlet weak var milage: UILabel!
    @IBOutlet weak var heart: UIButton!
    @IBOutlet weak var coverImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    deinit {
        print("deiniting Calist Cell")
    }
}
