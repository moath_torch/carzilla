//
//  FavoriteTableViewCell.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/16/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import UIKit

class FavoriteCellController: CarlistCellController {
    private var _cell: FavoriteTableViewCell {
     return self.cell as! FavoriteTableViewCell
    }
    weak var confirmableController: ConfirmPresentable?
    
    required init(model: Vehicle, cell: CarListTableViewCell) {
        super.init(model: model, cell: cell)
    }
    
    override func fill() {
       super.fill()
//        _cell.delete.setImage(R.image.delete(), for: .normal)
    }
    
    override func style() {
       super.style()
//        _cell.delete.setTitle("", for: .normal)
    }
    override func heartbuttonTapped(sender: Any) {
        assert(confirmableController != nil, "Make sure to add ConfirmPresentable viewcontroller/object")
        confirmableController?.showConfirmation(title: R.string.localizable.doYouWantToRemoveFromFavorites(model?.make ?? ""), ok: R.string.localizable.ok(), cancel: R.string.localizable.cancel(), okayed: { () in
            super.heartbuttonTapped(sender: sender)
        })
    }
}

class FavoriteTableViewCell: CarListTableViewCell {
    @IBOutlet weak var delete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
