
import UIKit
import ESTabBarController_swift

class TabbarItemBasicContentView: ESContentViewAdpater {
    let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize(width: 30.0, height: 30.0)))

    override init(frame: CGRect) {
        super.init(frame: frame)
        iconColor = UIColor.appBlue
        highlightIconColor = UIColor.appOrange
        
        view.layer.cornerRadius = 15.0
        view.alpha = 0
        view.backgroundColor = UIColor.clear
        self.insertSubview(view, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        view.center = self.center
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func selectAnimation(animated: Bool, completion: (() -> ())?) {
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: .allowAnimatedContent, animations: {
            self.view.alpha = 1
        }) { (b) in
            completion?()
        }
        
    }
    
    override func deselectAnimation(animated: Bool, completion: (() -> ())?) {
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: .allowAnimatedContent, animations: {
            self.view.alpha = 0
        }) { (b) in
            completion?()
        }
    }
}

