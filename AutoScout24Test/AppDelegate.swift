//
//  AppDelegate.swift
//  AutoScout24Test
//
//  Created by Moath_Othman on 6/15/17.
//  Copyright © 2017 Moath. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var starter: Starter = {
        return Starter(window: self.window)
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        starter.start()
        return true
    }
}
