//
//  Protocols.swift
//
//  Created by Moath_Othman on 11/8/16.
//  Copyright © 2016 Moath. All rights reserved.
//

import Foundation
import UIKit
// MARK: - Router
/// Any Router should conform to the Router Protocol, a Router should present,dismiss itself
protocol Router  {
    func present()
    func dismiss()
}

protocol CollectionRouter: Router  {
    func navigate(from index: IndexPath)
}

protocol KnownFromRouter: Router {
    associatedtype T: UIViewController
    weak var fromView: T? { get set }
    init(from: T?)
}

protocol CarryingRouter : Router {
    associatedtype DataType
    var carriedData: DataType { get set }
    init(carriedData: DataType, from view: UIViewController)
}

// MARK: - Presenter
/// Presenter that has carried data , fetchMethod
protocol Presenter {
    func fetchData()
}
/// Any view wishesh to be presentable should counform to Presntable (nsobject and the weak ability)
protocol Presentable: NSObjectProtocol {}

protocol DataSourcedPresenter: Presenter, UITableViewDataSource {}

protocol NonCarryingPresenter: Presenter {
    associatedtype Presentable
    init(view: Presentable)
    var view: Presentable { get }
}

protocol CarryingPresenter: Presenter {
    associatedtype Presentable
    associatedtype DataType
    var carriedObject: DataType { get set }
    init(carriedData: DataType, from view: Presentable)
}

protocol APIPresentable {
    func requestDidStart()
    func requestDidFinish()
}

protocol ErrorPresentable {
    func show(error msg: String?)
}

protocol ContactPresentable {
    func showContactForm()
}

protocol SuccessPresentable {
    func show(success msg: String?)
}

protocol ConfirmPresentable: NSObjectProtocol {
    func showConfirmation(title: String, ok: String, cancel: String, okayed: @escaping ((Void) -> ()))
    func showConfirmation(title: String?, message: String, ok: String, cancel: String, okayed: @escaping ((Void) -> ()))
}

// MARK: - Alerts
extension ConfirmPresentable where Self: UIViewController  {
    func showConfirmation(title: String, ok: String, cancel: String, okayed: @escaping ((Void) -> ())) {
        self.showConfirmation(title: nil, message: title, ok: ok, cancel: cancel, okayed: okayed)
    }
    
    func showConfirmation(title: String?, message: String, ok: String, cancel: String, okayed: @escaping ((Void) -> ())) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: { (ac) in
            okayed()
        }))
        alert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

protocol AlertPresentable: NSObjectProtocol {
    func alert(title: String, ok: String, okayed: @escaping ((Void) -> ()))
    func alert(title: String?, message: String, ok: String, okayed: @escaping ((Void) -> ()))
}

extension AlertPresentable where Self: UIViewController  {
    func alert(title: String, ok: String, okayed: @escaping ((Void) -> ())) {
        self.alert(title: nil, message: title, ok: ok, okayed: okayed)
    }
    
    func alert(title: String?, message: String, ok: String, okayed: @escaping ((Void) -> ())) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: { (ac) in
            okayed()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

/// StandardView protocol
protocol StandardView {
    func setup()
    func make() -> Self
    func lay() -> Self
    func style() -> Self
    func configure() // binding
}
extension StandardView where Self: UIView {
    func setup() {
        make().lay().style().configure()
    }
}

/// Status bar
protocol StatusBarBackground {
    func drawBackground(color: UIColor?)
}

extension StatusBarBackground where Self: UIViewController {
    func drawBackground(color: UIColor?) {
        let background = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 20))
        background.backgroundColor = color
        self.view.addSubview(background)
    }
}

/// Table view cell or collection view cell controller
protocol CellController {
    associatedtype Model
    associatedtype Cell
    /// reference to the cell
    var cell: Cell {get set}
    /// the model that ought to fill the cell
    var model: Model {get set}
    
    init(model: Model, cell: Cell)
    /// data filling code goes here
    func fill()
    /// cell styling goes here
    func style()
}

/// Table view cell or collection view cell controller
protocol WeakCellController {
    associatedtype Model
    associatedtype Cell: NSObjectProtocol
    /// reference to the cell
    weak var cell: Cell! {get set}
    /// the model that ought to fill the cell
    var model: Model {get set}
    
    init(model: Model, cell: Cell)
    /// data filling code goes here
    func fill()
    /// cell styling goes here
    func style()
}

protocol Localizable {
    func localize()
}
