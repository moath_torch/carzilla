
import Foundation
import UIKit


protocol Stylable {
    func style()
}
protocol Navigatable {}

extension Navigatable where Self: UIViewController {
    func setupBackButton() {
        self.navigationItem.backBarButtonItem = nil;
        let backButtonBackgroundImage = UIImage(named: "backarrow")?.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: -50, bottom: -5, right: -100))
        self.navigationController?.navigationBar.backIndicatorImage = backButtonBackgroundImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backButtonBackgroundImage
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    func setLeftButton(image: UIImage?, selector: Selector) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: image, style: .done, target: self, action: selector)
    }
    
    func setRightButton(image: UIImage?, selector: Selector) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: image, style: .done, target: self, action: selector)
    }
    
    func setRightButton(title: String?, selector: Selector) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: .done, target: self, action: selector)
    }
    
    func enableRightButton(enabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = enabled
    }
    
    func enableLeftButton(enabled: Bool) {
        navigationItem.leftBarButtonItem?.isEnabled = enabled
    }
}

// MARK: - Cores Controllers

class CoreViewController: UIViewController, Navigatable {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBackButton()
        if self is Stylable {
            (self as! Stylable).style()
        }
    }
}

class CoreTableViewController: UITableViewController, Navigatable {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBackButton()
        if self is Stylable {
            (self as! Stylable).style()
        }
        if self is Localizable {
            (self as! Localizable).localize()
        }
    }
}

/// MVPR ViewController with Presenter and Router requirement.
/// a subclass should have a Presenter and a Router.
class MVPRViewController<P: Presenter, R: Router>: CoreViewController {
    var presenter: P!
    var router: R!
    
    public var delayed: () -> Void = {}
    public func setDelayed(listener: @escaping () -> Void) {
        delayed = listener
    }
    public func dispose() { delayed = {} }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
    }
    
    deinit {
        print("deiniting MVPRViewController \(self) ")
    }
}

/**
 *   description: MVPR TableVieController , add datasource requirement.
 *   subclass should have a Presenter and a Router.
 *   subclass Presenter should conform to the tableviewdatasource.
 *
 *   **auther**: Moath
 */
class MVPRDViewController<P: DataSourcedPresenter, R: Router>: CoreViewController  {
    var presenter: P!
    var router: R!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
    }
    
    deinit {
        print("deiniting MVPRDTableViewController \(self)")
    }
}

/**
  *   description: MVPR TableVieController , add datasource requirement.
  *   subclass should have a Presenter and a Router.
  *   subclass Presenter should conform to the tableviewdatasource.
  *
  *   **auther**: Moath
 */
class MVPRDTableViewController<P: Presenter & UITableViewDataSource, R: Router>: CoreTableViewController  {
    var presenter: P!
    var router: R!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
        self.setupBackButton()
    }
    
    deinit {
        print("deiniting MVPRDTableViewController \(self)")
    }
}

/**
 *   description: MVPR TableVieController , no datasource requirement.
 *   subclass should have a Presenter and a Router.
 *   used when delaing with static cells
 *   **auther**: Moath
 */
class MVPRTableViewController<P: Presenter , R: Router>: CoreTableViewController  {
    var presenter: P!
    var router: R!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
        self.setupBackButton()
    }
    
    deinit {
        print("deiniting MVPRTableViewController \(self)")
    }
}


/**
 *   description: MVPR TableVieController , no datasource requirement.
 *   subclass should have a Presenter and a Router.
 *   used when delaing with static cells
 *   **auther**: Moath
 */
class MVPRTabBarController<P: Presenter , R: Router>: UITabBarController  {
    var presenter: P!
    var router: R!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard presenter != nil else {
            assert(false, "Presenter should be assigned to this Viewcontroller")
            fatalError()
        }
        guard router != nil else {
            assert(false, "Router should be assigned to this Viewcontroller")
            fatalError()
        }
        presenter.fetchData()
    }
    
    deinit {
        print("deiniting MVPRTabBarController \(self)")
    }
}


class MVPRRouter: NSObject {
    deinit {
        print("deiniting router \(self)")
    }
}

class MVPRPresenter: NSObject {
    deinit {
        print("deiniting presenter \(self)")
    }
}
