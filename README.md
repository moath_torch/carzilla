# Carzill #

An App to view used cars.
# Architecture
MVP-R 
#Structure 
-- Pods (dependencies e.g. alamofire)

-- Project

|---Utilities (extension ,mainly styles for color/font..)

|---Scenes (MVP stacks)

|---Services (e.g db )

|---AppCore (main superclasses and protocols)

|---UI (storyboards,xib, views)

|---Resources (fonts)

|---Models (vehicle)

|---API (apiclient and services)
#features
* (**Data handling**) Introduce a validation feature. This feature postprocesses the recieved list and grays out all invalid vehicles. A vehicle is invalid when the AccidentFree property is false. Prevent users from adding an invalid car to the favorites list.
* (**Data handling**) Utilize the make parameter of the datasource of the car list screen. It can have 4 values: all, bmw, audi, mercedes-benz. The app should pick cyclically one out of this 4 with every refresh
(http://sumamo.de/iOS-TechChallange/api/index/make=[all | bmw | audi | mercedes-benz].json). Make sure, that the favorite [switches](https://developer.apple.com/ios/human-interface-guidelines/ui-controls/switches/). reflect the proper state for each car after the list is refreshed.
* (**Networking**) Make sure that each and every response of a request ignores locally or remotely cached data.
* (**UI**) Make sure that all of the assets (labels, images) are displayed nicely (no cuts, same size). The app should reflect an easy to use and consistent interface.

* (**Multithreading**) Show the first image of each vehicle. Get the image in an asynchronous way.
* (**Architecture**) Add a badge to the tab bar favorites list item. The badge should represent the number of vehicles that have been added to the favorites list since it was last viewed. When the user switches to the favorites list, the badge should disappear.